import Foundation

// Global flag indicating a connection error
var connectionError = false

/// Generic function to perform HTTP requests with an Encodable payload.
/// - Parameters:
///   - payload: Data to send (encoded either as JSON or URL-encoded if [String: String])
///   - headers: HTTP headers to include in the request
///   - address: URL string of the endpoint
///   - method: HTTP method (e.g., "GET", "POST")
///   - completion: Callback returning response data and HTTP status code
func requests<T: Encodable>(payload: T, headers: [String: String], address: String, method: String, completion: @escaping (Data, Int) -> ()) {
    // Ensure URL is valid
    guard let url = URL(string: address) else {
        print("Invalid URL: \(address)")
        return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = method
    
    // For non-GET methods, set the HTTP body
    if method != "GET" {
        // If payload is a dictionary of strings, encode as URL-encoded form data
        if let payloadDict = payload as? [String: String] {
            let bodyComponents = payloadDict.map { key, value in
                "\(key)=\(urlEncode(rawText: value))"
            }.joined(separator: "&")
            request.httpBody = bodyComponents.data(using: .utf8)
        } else {
            // Otherwise, encode the payload as JSON
            request.httpBody = try? JSONEncoder().encode(payload)
        }
    }
    
    // Add provided headers to the request
    headers.forEach { field, value in
        request.addValue(value, forHTTPHeaderField: field)
    }
    
    // Create data task
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data,
              let httpResponse = response as? HTTPURLResponse,
              error == nil else {
            print("Request error: \(error?.localizedDescription ?? "Unknown error")")
            return
        }
        completion(data, httpResponse.statusCode)
    }
    task.resume()
}

/// Synchronous login function using a semaphore to wait for the network response.
/// - Parameters:
///   - username: User's username
///   - password: User's password
/// - Returns: A tuple containing the access token and the HTTP response code.
func login(username: String, password: String) -> (String, Int) {
    struct Login: Decodable {
        var access_token: String
        var token_type: String
    }
    
    var loginResult: Login = Login(access_token: "", token_type: "")
    var responseCode = 0
    let payload = ["username": username, "password": password]
    let headers = ["Accept": "application/json", "Content-Type": "application/x-www-form-urlencoded"]
    connectionError = false
    
    // Use a semaphore to wait for the asynchronous request
    let semaphore = DispatchSemaphore(value: 0)
    
    requests(payload: payload, headers: headers, address: loginEndpoint, method: "POST") { data, response in
        responseCode = response
        if response == 200 {
            do {
                loginResult = try JSONDecoder().decode(Login.self, from: data)
            } catch {
                print("Login decoding error: \(error)")
            }
        }
        semaphore.signal()
    }
    
    // Wait for up to 5 seconds for the network response
    if semaphore.wait(timeout: .now() + 5) == .timedOut {
        connectionError = true
    }
    
    return (loginResult.access_token, responseCode)
}

/// Synchronous function to check if the user profile is active, with up to 3 retries.
/// - Parameter token: User's authentication token.
/// - Returns: A Boolean indicating whether the user is active.
func checkUserProfile(token: String) -> Bool {
    struct User: Decodable {
        var id: Int
        var username: String
        var device_tokens: [String]
        var is_active: Bool
        var is_admin: Bool
    }
    
    var userProfile = User(id: 0, username: "", device_tokens: [], is_active: false, is_admin: false)
    let headers = ["Accept": "application/json", "Authorization": "Bearer " + token]
    let maxRetries = 3
    var attempt = 0
    
    while attempt < maxRetries {
        let semaphore = DispatchSemaphore(value: 0)
        connectionError = false
        
        requests(payload: "", headers: headers, address: profileEndpoint, method: "GET") { data, response in
            if response == 200 {
                do {
                    userProfile = try JSONDecoder().decode(User.self, from: data)
                } catch {
                    print("User profile decoding error: \(error)")
                }
            }
            semaphore.signal()
        }
        
        // Wait up to 5 seconds for a response
        if semaphore.wait(timeout: .now() + 5) == .timedOut {
            connectionError = true
            attempt += 1
            print("Attempt \(attempt) timed out, retrying...")
            Thread.sleep(forTimeInterval: 1.0) // Short pause before retrying
            continue
        }
        
        return userProfile.is_active
    }
    
    // If all attempts fail, return false
    return false
}

/// Sets the APN (Apple Push Notification) token on the server.
/// - Parameters:
///   - token: User's authentication token.
///   - apn: The device's APN token.
func setAPNToken(token: String, apn: String) {
    struct APN: Encodable {
        var device_token: String
    }
    
    let payload = APN(device_token: apn)
    let headers = ["Accept": "application/json", "Content-Type": "application/json", "Authorization": "Bearer " + token]
    
    requests(payload: payload, headers: headers, address: apnEndpoint, method: "POST") { data, response in
        if response == 401 {
            invalidToken = true
        }
    }
}

/// Fetches available services from the server and updates the global `userServices` variable.
/// - Parameter token: User's authentication token.
func getServices(token: String) {
    let headers = ["Accept": "application/json", "Content-Type": "application/json", "Authorization": "Bearer " + token]
    
    requests(payload: "", headers: headers, address: servicesEndpoint, method: "GET") { data, response in
        if response == 200 {
            do {
                userServices = try JSONDecoder().decode([UserService].self, from: data)
            } catch {
                print("Services decoding error: \(error)")
            }
        }
        if response == 401 {
            invalidToken = true
        }
    }
}

/// Sets user subscriptions on the server for a list of services.
/// - Parameters:
///   - token: User's authentication token.
///   - services: Dictionary mapping service IDs to subscription status.
func setSubscriptions(token: String, services: [String: Bool]) {
    struct Service: Encodable {
        var id: String
        var is_subscribed: Bool
    }
    
    var payload = [Service]()
    for (serviceId, isSubscribed) in services {
        payload.append(Service(id: serviceId, is_subscribed: isSubscribed))
    }
    let headers = ["Accept": "application/json", "Content-Type": "application/json", "Authorization": "Bearer " + token]
    
    requests(payload: payload, headers: headers, address: servicesEndpoint, method: "PATCH") { data, response in
        if response == 401 {
            invalidToken = true
        }
    }
}

/// Fetches notifications from the server and updates the global `userNotifications` variable.
/// - Parameter token: User's authentication token.
func getNotifications(token: String) {
    let headers = ["Accept": "application/json", "Content-Type": "application/json", "Authorization": "Bearer " + token]
    
    requests(payload: "", headers: headers, address: notificationsEndpoint, method: "GET") { data, response in
        if response == 200 {
            do {
                userNotifications = try JSONDecoder().decode([UserNotification].self, from: data)
            } catch {
                print("Notifications decoding error: \(error)")
            }
        }
        if response == 401 {
            invalidToken = true
        }
    }
}

/// Sets notification statuses (e.g., read, unread, deleted) on the server and updates the global notifications list accordingly.
/// - Parameters:
///   - token: User's authentication token.
///   - notifications: Dictionary mapping notification IDs to their new status.
func setNotifications(token: String, notifications: [Int: String]) {
    struct Notification: Encodable {
        var id: Int
        var status: String
    }
    
    var payload = [Notification]()
    for (notificationId, status) in notifications {
        payload.append(Notification(id: notificationId, status: status))
    }
    let headers = ["Accept": "application/json", "Content-Type": "application/json", "Authorization": "Bearer " + token]
    
    requests(payload: payload, headers: headers, address: notificationsEndpoint, method: "PATCH") { data, response in
        if response == 401 {
            invalidToken = true
        }
    }
    
    // Update the global userNotifications array based on status changes
    var updatedNotifications = [UserNotification]()
    for var notification in userNotifications {
        var include = true
        for (id, status) in notifications {
            if notification.id == id {
                if status == "deleted" {
                    include = false
                }
                if status == "read" {
                    notification.is_read = true
                }
                if status == "unread" {
                    notification.is_read = false
                }
            }
        }
        if include {
            updatedNotifications.append(notification)
        }
    }
    userNotifications = updatedNotifications
}

/// Encodes a raw string for safe use in a URL query parameter.
/// - Parameter rawText: The text to encode.
/// - Returns: The URL-encoded string.
func urlEncode(rawText: String) -> String {
    let encodingTable = ["%": "%25", " ": "%20", "<": "%3C", ">": "%3E",
                         "#": "%23", "+": "%2B", "{": "%7B", "}": "%7D",
                         "|": "%7C", "\\": "%5C", "^": "%5E", "~": "%7E",
                         "[": "%5B", "]": "%5D", "‘": "%60", ";": "%3B",
                         "/": "%2F", "?": "%3F", ":": "%3A", "@": "%40",
                         "=": "%3D", "&": "%26", "$": "%24"]
    var encodedText = rawText
    for (key, value) in encodingTable {
        encodedText = encodedText.replacingOccurrences(of: key, with: value)
    }
    return encodedText
}
