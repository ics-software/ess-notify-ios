//
//  Share.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-11-18.
//

import SwiftUI

/// A SwiftUI wrapper for presenting the native share sheet.
struct ShareSheet: UIViewControllerRepresentable {
    /// Completion handler for the share activity.
    typealias Callback = (_ activityType: UIActivity.ActivityType?, _ completed: Bool, _ returnedItems: [Any]?, _ error: Error?) -> Void
    
    /// Items to share.
    let activityItems: [Any]
    /// Optional activities to include.
    let applicationActivities: [UIActivity]? = nil
    /// Activities to exclude from the share sheet.
    let excludedActivityTypes: [UIActivity.ActivityType]? = nil
    /// Optional callback handler.
    let callback: Callback? = nil
    
    /// Creates and configures the UIActivityViewController.
    func makeUIViewController(context: Context) -> UIActivityViewController {
        let controller = UIActivityViewController(
            activityItems: activityItems,
            applicationActivities: applicationActivities
        )
        controller.excludedActivityTypes = excludedActivityTypes
        controller.completionWithItemsHandler = callback
        return controller
    }
    
    /// No updates are required after the controller is created.
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: Context) { }
}
