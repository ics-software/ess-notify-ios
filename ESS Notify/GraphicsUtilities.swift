//
//  Graphics.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-10-22.
//

import Foundation
import SwiftUI

extension Color {
    init(hex: String) {
        // Rimuove caratteri non validi e utilizza una variabile "sanitized" per chiarezza
        let sanitized = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: sanitized).scanHexInt64(&int)
        let a, r, g, b: UInt64
        
        switch sanitized.count {
        case 3: // RGB (12-bit)
            // Ogni componente è moltiplicata per 17 per convertire i valori a 8-bit
            (a, r, g, b) = (255,
                            (int >> 8 & 0xF) * 17,
                            (int >> 4 & 0xF) * 17,
                            (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255,
                            (int >> 16) & 0xFF,
                            (int >> 8) & 0xFF,
                            int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = ((int >> 24) & 0xFF,
                            (int >> 16) & 0xFF,
                            (int >> 8) & 0xFF,
                            int & 0xFF)
        default:
            // In caso di stringa non valida, restituisce un colore trasparente
            self = Color.clear
            return
        }
        
        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue: Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
}

struct CustomTextField: View {
    var placeholder: Text
    @Binding var text: String
    var editingChanged: (Bool) -> Void = { _ in }
    var commit: () -> Void = { }
    
    var body: some View {
        ZStack(alignment: .leading) {
            if text.isEmpty { placeholder }
            TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit)
        }
    }
}
