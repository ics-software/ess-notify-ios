//
//  ServicesView.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-11-18.
//

import SwiftUI

struct ServicesView: View {
    @Binding var screenSelector: String
    @State var serviceList = userServices
    @State private var selection = ""
    
    var body: some View {
        VStack {
            // Header title for available notification services
            Text("Available Notification Services")
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 7)
            
            // Search bar: Adjusted with horizontal padding to match the width of the subscription buttons
            CustomTextField(placeholder: Text("Search...").foregroundColor(.gray),
                            text: $selection)
                .padding(7)
                .background(searchColor)
                .foregroundColor(.black)
                .cornerRadius(8)
                .autocapitalization(.none)
                .padding(.horizontal, 20) // Adding horizontal padding to mimic the list row insets
            
            // List of services filtered by the search text
            List {
                ForEach(0..<serviceList.count, id: \.self) { i in
                    if serviceList[i].category.lowercased().contains(selection.lowercased()) || selection == "" {
                        Button(action: {
                            // Toggle subscription status
                            serviceList[i].is_subscribed.toggle()
                            userServices[i].is_subscribed.toggle()
                            setSubscriptions(token: userData.ESSToken, services: [userServices[i].id: userServices[i].is_subscribed])
                        }) {
                            HStack {
                                // Display subscription icon based on current subscription status
                                if serviceList[i].is_subscribed {
                                    Image("icon-selected")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(minWidth: 0, maxWidth: 20)
                                        .foregroundColor(.white)
                                } else {
                                    Image(systemName: "square")
                                        .foregroundColor(.white)
                                }
                                Spacer()
                                // Display the service category name
                                Text(serviceList[i].category)
                                    .foregroundColor(.white)
                                Spacer()
                            }
                        }
                        .listRowBackground(Color(hex: "#" + serviceList[i].color))
                        .listRowSeparator(.hidden)
                    }
                }
            }
            .scrollContentBackground(.hidden)
            
            // Save button to confirm subscription changes
            Button(action: {
                withAnimation(.easeOut(duration: 0.3)) {
                    self.screenSelector = "notifications"
                }
            }) {
                HStack {
                    Image("icon-save")
                        .resizable()
                        .scaledToFit()
                        .frame(minWidth: 0, maxWidth: 30)
                        .foregroundColor(.white)
                    Text("Save")
                }
            }
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding(.bottom, 10)
            .foregroundColor(Color.white)
        }
        .onAppear() {
            // Load available services when the view appears
            getServices(token: userData.ESSToken)
            serviceList = userServices
        }
    }
}
