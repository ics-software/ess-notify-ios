//
//  Notifications.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-11-18.
//

import Foundation

/// Returns the color (as a hex string) for a notification based on its associated service.
/// - Parameter Index: The ID of the notification.
/// - Returns: A hex color string (e.g., "#FF0000") if found, otherwise "#000000".
func getColorNotification(Index: Int) -> String {
    if let notification = userNotifications.first(where: { $0.id == Index }),
       let service = userServices.first(where: { $0.id == notification.service_id }) {
        return "#" + service.color
    }
    return "#000000"
}

/// Returns the service category for a notification.
/// - Parameter Index: The ID of the notification.
/// - Returns: The service category if found, otherwise an empty string.
func getServiceCategory(Index: Int) -> String {
    if let notification = userNotifications.first(where: { $0.id == Index }),
       let service = userServices.first(where: { $0.id == notification.service_id }) {
        return service.category
    }
    return ""
}

/// Builds a dictionary mapping service IDs to their categories for services that are associated with notifications.
/// - Returns: A dictionary where keys are service IDs and values are their categories.
func getNotificationsColors() -> [String: String] {
    // Create a set of service IDs from the notifications
    let serviceIds = Set(userNotifications.map { $0.service_id })
    var colors = [String: String]()
    // Iterate over services and include only those referenced by a notification
    for service in userServices where serviceIds.contains(service.id) {
        colors[service.id] = service.category
    }
    return colors
}

/// Performs a bulk action (e.g., marking as read or deleting) on notifications for a given service.
/// If "any" is passed as the service_id, applies the action to all notifications.
/// - Parameters:
///   - service_id: The service ID or "any" for all notifications.
///   - action: The action to apply (e.g., "read", "deleted").
func bulkAction(service_id: String, action: String) {
    let todo = userNotifications
        .filter { service_id == "any" || $0.service_id == service_id }
        .reduce(into: [Int: String]()) { dict, notification in
            dict[notification.id] = action
        }
    setNotifications(token: userData.ESSToken, notifications: todo)
}

/// Converts an ISO8601 timestamp to a more user-friendly string format.
/// - Parameter timestamp: The original timestamp string.
/// - Returns: A formatted date string, or the original timestamp if conversion fails.
func convertTimeFormat(timestamp: String) -> String {
    let inFormat = DateFormatter()
    inFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    
    // Ensure the timestamp ends with "Z" to match the expected format.
    let timestampWithZ = timestamp.last == "Z" ? timestamp : timestamp + "Z"
    
    guard let date = inFormat.date(from: timestampWithZ) else {
        // Fallback: return the original timestamp if conversion fails.
        return timestamp
    }
    
    let outFormat = DateFormatter()
    outFormat.dateFormat = "MMM d, yyyy 'at' HH:mm:ss"
    return outFormat.string(from: date)
}
