//
//  LoginUtilities.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-11-18.
//

import Foundation

/// Loads the user's credentials from the documents directory and applies customizations based on the saved laboratory.
func loadCredentials() {
    do {
        // Retrieve the documents directory URL and build the credentials file URL.
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let credentialsURL = documentsDirectory.appendingPathComponent("credentials")
        
        // Load data from the credentials file and decode it into a UserData object.
        let data = try Data(contentsOf: credentialsURL)
        userData = try JSONDecoder().decode(UserData.self, from: data)
        
        // Apply UI customizations based on the saved laboratory.
        applyCustomization(laboratory: userData.server)
    } catch {
        print("Load failed: \(error)")
    }
}

/// Saves the user's credentials to the documents directory.
func saveCredentials() {
    do {
        // Encode the userData object into JSON data.
        let data = try JSONEncoder().encode(userData)
        
        // Retrieve the documents directory URL and build the credentials file URL.
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let credentialsURL = documentsDirectory.appendingPathComponent("credentials")
        
        // Write the encoded data to the file.
        try data.write(to: credentialsURL)
    } catch {
        print("Save failed: \(error)")
    }
}

/// Checks the validity of the user's credentials and returns the appropriate screen identifier.
/// - Returns: "notifications" if the credentials are valid; otherwise, "login".
func checkCredentials() -> String {
    // If the token is empty, the user needs to log in.
    guard !userData.ESSToken.isEmpty else {
        return "login"
    }
    
    // Check the user profile with the current token.
    if checkUserProfile(token: userData.ESSToken) {
        invalidToken = false
        return "notifications"
    }
    return "login"
}

/// Attempts to log in using the provided username and password.
/// - Parameters:
///   - username: The user's username.
///   - password: The user's password.
/// - Returns: A Boolean value indicating whether the login was successful.
func checkLogin(username: String, password: String) -> Bool {
    let (token, response) = login(username: username, password: password)
    if response == 200 {
        userData.ESSToken = token
        saveCredentials()
        return true
    }
    return false
}
