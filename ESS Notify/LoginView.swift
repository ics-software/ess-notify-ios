//
//  Login.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-10-22.
//

import SwiftUI

struct LoginView: View {
    @Binding var screenSelector: String
    @State private var username = ""
    @State private var password = ""
    @State private var errorLogin = false
    @State private var buttonLogin = "Login"
    @State private var labSwitch = false
    
    // Colors for lab labels
    @State private var colorESSLabel = Color.white
    @State private var colorMAXIVLabel = Color(red: 0.33, green: 0.37, blue: 0.40, opacity: 1.0)
    
    var body: some View {
        VStack {
            // Lab logo
            Image(labLogo)
                .resizable()
                .scaledToFit()
                .frame(maxWidth: .infinity, maxHeight: 150)
                .padding(10)
            
            Spacer()
            
            // Lab selection
            labSelectionView
            
            Spacer()
            
            // Username field
            TextField("Username", text: $username)
                .autocapitalization(.none)
                .textFieldStyleCustom()
            
            Spacer()
            
            // Password field
            SecureField("Password", text: $password)
                .textFieldStyleCustom()
            
            Spacer()
            
            // Login button
            Button(action: loginAction) {
                Text(buttonLogin)
                    .frame(maxWidth: .infinity)
            }
            .padding()
            .background(cellColor)
            .cornerRadius(30)
            .foregroundColor(.white)
            .alert(isPresented: $errorLogin) {
                if connectionError {
                    return Alert(title: Text("Network Error"),
                                 message: Text("It was not possible to contact the Notify server"),
                                 dismissButton: .default(Text("Try Again")))
                } else {
                    return Alert(title: Text("Error"),
                                 message: Text("Wrong Username or Password"),
                                 dismissButton: .default(Text("Try Again")))
                }
            }
            
            Spacer()
        }
        .onAppear {
            // Set the toggle based on the saved server
            labSwitch = (userData.server == "maxiv")
        }
    }
    
    // MARK: - Subview for Lab Selection
    
    private var labSelectionView: some View {
        HStack {
            Spacer()
            Text("ESS")
                .font(.title2)
                .padding()
                .foregroundColor(colorESSLabel)
            
            Toggle("", isOn: $labSwitch)
                .padding()
                .labelsHidden()
                .toggleStyle(SwitchToggleStyle(tint: Color(red: 0.21, green: 0.24, blue: 0.27, opacity: 1.0)))
                .onChange(of: labSwitch) { newValue in
                    withAnimation(.easeOut(duration: 0.5)) {
                        updateLabSelection(newValue: newValue)
                    }
                }
            
            Text("MAX IV")
                .font(.title2)
                .padding()
                .foregroundColor(colorMAXIVLabel)
            Spacer()
        }
    }
    
    // MARK: - Actions
    
    /// Updates the lab selection and applies customizations
    private func updateLabSelection(newValue: Bool) {
        if newValue {
            userData.server = "maxiv"
            colorMAXIVLabel = .white
            colorESSLabel = Color(red: 0.33, green: 0.37, blue: 0.40, opacity: 1.0)
        } else {
            userData.server = "ess"
            colorESSLabel = .white
            colorMAXIVLabel = Color(red: 0.33, green: 0.37, blue: 0.40, opacity: 1.0)
        }
        saveCredentials()
        applyCustomization(laboratory: userData.server)
        // Refresh the login screen (temporary switch to splash)
        screenSelector = "splash"
        screenSelector = "login"
    }
    
    /// Performs the login action
    private func loginAction() {
        buttonLogin = "Logging in..."
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if checkLogin(username: username, password: password) {
                invalidToken = false
                screenSelector = "notifications"
            } else {
                buttonLogin = "Login"
                errorLogin = true
                screenSelector = "login"
            }
        }
    }
}

// MARK: - Custom TextField Modifier

extension View {
    func textFieldStyleCustom() -> some View {
        self
            .padding()
            .font(.title2)
            .multilineTextAlignment(.center)
            .background(bgColor)
            .border(cellColor, width: 3)
            .foregroundColor(.white)
    }
}
