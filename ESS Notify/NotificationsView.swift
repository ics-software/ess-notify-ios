//
//  Login.swift
//  ESS Notify
//
//  Created by Emanuele Laface on 2020-10-22.
//

import SwiftUI
import UserNotifications

struct NotificationsView: View {
    @Binding var screenSelector: String
    @State private var currentTime = Date()
    @State private var notifications = userNotifications
    @State private var currentService = "any"
    @State private var showReadAllAlert = false
    @State private var showDeleteAllAlert = false
    @State private var detailedNotification: UserNotification? = nil
    @State private var showShareSheet = false
    @State private var badgeCounter = 0

    let timer = Timer.publish(every: 1, on: .main, in: .default).autoconnect()

    // Filter notifications based on the selected service
    var filteredNotifications: [UserNotification] {
        notifications.filter { currentService == "any" || $0.service_id == currentService }
    }

    var body: some View {
        VStack(spacing: 0) {
            headerView
            List {
                if filteredNotifications.isEmpty {
                    Section(header: Text("")) {}.listRowBackground(bgColor)
                } else {
                    ForEach(Array(filteredNotifications.enumerated()), id: \.element.id) { index, notification in
                        notificationGroup(for: notification, isLast: index == filteredNotifications.count - 1)
                    }
                    .onDelete(perform: deleteNotifications)
                }
            }
            .scrollContentBackground(.hidden)
            .environment(\.defaultMinListRowHeight, 20)
            .refreshable {
                // Perform refresh by temporarily switching to the "splash" screen
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    screenSelector = "splash"
                    screenSelector = "notifications"
                }
            }
            bottomToolbarView
        }
        .onAppear(perform: setupView)
        .onReceive(timer) { newTime in
            updateNotifications(newTime: newTime)
        }
        .sheet(item: $detailedNotification) { notification in
            detailedNotificationSheet(notification: notification)
        }
    }

    // MARK: - Subviews

    private var headerView: some View {
        ZStack(alignment: .top) {
            bgColor
            Image("splash-logo")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 50) // Logo height
        }
        .frame(height: 50) // Fixed container height equal to the logo
    }

    // Group together the header, content, and spacer for each notification
    private func notificationGroup(for notification: UserNotification, isLast: Bool) -> some View {
        let serviceColor = Color(hex: getColorNotification(Index: notification.id))
        return Group {
            // Header row with the service title
            HStack {
                Text(getServiceCategory(Index: notification.id))
                    .lineLimit(1)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .font(.system(size: 20))
                    .padding(1)
                Spacer()
            }
            .listRowBackground(serviceColor)
            .deleteDisabled(true)
            
            // Content rows of the notification
            Button(action: {
                markNotificationAsRead(notification)
                detailedNotification = notification
            }) {
                VStack(alignment: .leading, spacing: 4) {
                    HStack(alignment: .top) {
                        if !notification.is_read {
                            Button(action: {
                                markNotificationAsRead(notification)
                            }) {
                                Image(systemName: "circlebadge.fill")
                                    .foregroundColor(.red)
                            }
                            .buttonStyle(PlainButtonStyle())
                        }
                        Text(.init(notification.title))
                            .font(.system(size: 18, weight: .semibold))
                            .foregroundColor(.white)
                            .lineLimit(1)
                    }
                    HStack {
                        Spacer()
                        Text(convertTimeFormat(timestamp: notification.timestamp))
                            .font(.footnote)
                            .foregroundColor(.white)
                    }
                    Text(.init(notification.subtitle))
                        .font(.system(size: 16))
                        .foregroundColor(.white)
                        .lineLimit(2)
                }
                .padding(5)
            }
            .listRowBackground(cellColor)
            
            // Spacer to separate notifications (if it's not the last one)
            if !isLast {
                Spacer()
                    .listRowBackground(bgColor)
                    .deleteDisabled(true)
            }
        }
    }

    private var bottomToolbarView: some View {
        HStack {
            Menu {
                Picker(selection: $currentService, label: Text("")) {
                    ForEach(getNotificationsColors().sorted(by: <), id: \.key) { key, value in
                        Text(value).tag(key)
                    }
                    Text("All").tag("any")
                }
            } label: {
                Image("icon-filter")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 30)
                    .foregroundColor(.white)
            }
            .padding(.horizontal)

            Spacer()

            Button(action: {
                if !filteredNotifications.isEmpty { showReadAllAlert = true }
            }) {
                Image("icon-unread")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 30)
                    .foregroundColor(.white)
            }
            .padding(.horizontal)
            .alert(isPresented: $showReadAllAlert) {
                Alert(
                    title: Text("Read All"),
                    message: Text("Do you want to mark the current messages as read?"),
                    primaryButton: .default(Text("Mark as Read"), action: {
                        bulkAction(service_id: currentService, action: "read")
                        showReadAllAlert = false
                        currentService = "any"
                    }),
                    secondaryButton: .cancel({ showReadAllAlert = false })
                )
            }

            Spacer()

            Button(action: {
                if !filteredNotifications.isEmpty { showDeleteAllAlert = true }
            }) {
                Image("icon-trash")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 30)
                    .foregroundColor(.white)
            }
            .padding(.horizontal)
            .alert(isPresented: $showDeleteAllAlert) {
                Alert(
                    title: Text("Delete All"),
                    message: Text("Do you want to delete all the current messages?"),
                    primaryButton: .default(Text("Delete All"), action: {
                        bulkAction(service_id: currentService, action: "deleted")
                        showDeleteAllAlert = false
                        currentService = "any"
                    }),
                    secondaryButton: .cancel({ showDeleteAllAlert = false })
                )
            }

            Spacer()

            Button(action: {
                withAnimation(.easeOut(duration: 0.3)) {
                    screenSelector = "services"
                }
            }) {
                Image("icon-settings")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 30)
                    .foregroundColor(.white)
            }
            .padding(.horizontal)

            Spacer()

            Button(action: {
                withAnimation(.easeOut(duration: 0.3)) {
                    screenSelector = "info"
                }
            }) {
                Image("icon-information")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 30)
                    .foregroundColor(.white)
            }
            .padding(.horizontal)
        }
        .padding(.bottom, 8)
    }

    private func detailedNotificationSheet(notification: UserNotification) -> some View {
        VStack {
            HStack {
                Spacer()
                Text(getServiceCategory(Index: notification.id))
                    .font(.system(size: 20))
                    .padding(.vertical, 5)
                    .padding(.leading, 30)
                Spacer()
                Button(action: { showShareSheet = true }) {
                    Image(systemName: "square.and.arrow.up")
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: 20)
                        .foregroundColor(.white)
                        .padding(.trailing, 10)
                }
            }
            .frame(maxHeight: 40, alignment: .topLeading)
            .background(Color(hex: getColorNotification(Index: notification.id)))
            
            Text(.init(notification.title))
                .font(.system(size: 18, weight: .semibold))
                .foregroundColor(.white)
                .multilineTextAlignment(.center)
                .padding(.vertical, 10)
                .frame(width: 330)
            Divider()
            Text(convertTimeFormat(timestamp: notification.timestamp))
                .font(.footnote)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding(.trailing, 10)
            List {
                VStack(alignment: .leading, spacing: 8) {
                    Text(.init(notification.subtitle))
                        .font(.system(size: 16))
                        .foregroundColor(.white)
                        .multilineTextAlignment(.leading)
                    if !notification.url.isEmpty {
                        Divider()
                        Button(action: {
                            if let url = URL(string: notification.url) {
                                UIApplication.shared.open(url)
                            }
                        }) {
                            Text(.init(notification.url))
                                .font(.system(size: 16))
                        }
                        .buttonStyle(PlainButtonStyle())
                        .frame(maxWidth: .infinity, alignment: .leading)
                    }
                }
                .listRowBackground(bgColor)
            }
            .scrollContentBackground(.hidden)
            .listRowSeparator(.hidden)
        }
        .background(bgColor)
        .sheet(isPresented: $showShareSheet) {
            let message = """
            \(convertTimeFormat(timestamp: notification.timestamp))
            Notify Service: \(getServiceCategory(Index: notification.id))
            Title: \(notification.title)
            - - - - -
            \(notification.subtitle)
            - - - - -
            \(notification.url)
            """
            ShareSheet(activityItems: [message])
        }
    }

    // MARK: - Helper Functions

    private func setupView() {
        UITableView.appearance().backgroundColor = .clear
        registerForPushNotifications()
        getNotifications(token: userData.ESSToken)
        getServices(token: userData.ESSToken)
    }

    private func updateNotifications(newTime: Date) {
        if newTime.timeIntervalSince(currentTime) > 2 {
            getNotifications(token: userData.ESSToken)
        }
        if userNotifications.isEmpty {
            currentService = "any"
        }
        badgeCounter = userNotifications.reduce(0) { $0 + (!$1.is_read ? 1 : 0) }
        notifications = userNotifications.filter { currentService == "any" || $0.service_id == currentService }
        UNUserNotificationCenter.current().setBadgeCount(badgeCounter)
        if invalidToken {
            userData.ESSToken = ""
            screenSelector = "login"
        }
        currentTime = newTime
    }

    private func markNotificationAsRead(_ notification: UserNotification) {
        guard !notification.is_read else { return }
        if let index = notifications.firstIndex(where: { $0.id == notification.id }) {
            notifications[index].is_read = true
        }
        setNotifications(token: userData.ESSToken, notifications: [notification.id: "read"])
    }

    private func deleteNotifications(at offsets: IndexSet) {
        for index in offsets {
            let notification = filteredNotifications[index]
            setNotifications(token: userData.ESSToken, notifications: [notification.id: "deleted"])
        }
        withAnimation {
            notifications.removeAll { notif in
                offsets.contains(filteredNotifications.firstIndex(where: { $0.id == notif.id }) ?? -1)
            }
        }
    }
}
